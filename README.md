denkmal.org 2010-2014
=====================

Historical repo of denkmal.org.
- **Functionality**: List of daily events, audio player, *song of the moment*, mobile version, ticket lottery.
- **Technology**: PHP application based on [Zend Framework 1](https://github.com/zendframework/zf1).

---
![Screenshot 1](docs/screenshot1.png)
---
![Screenshot 2](docs/screenshot2.png)
---
![Screenshot 3](docs/screenshot3.png)
